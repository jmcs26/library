package com.training.citi.library.service;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import com.training.citi.library.dao.LibraryMongoRepo;
import com.training.citi.library.model.Item;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

/**
 * @author James McSherry
 * 
 *Test class that uses a mock mongo repo
 */

@SpringBootTest
public class LibraryServiceMockingTests {


    @Autowired
    private LibraryService libraryService;

    @MockBean
    private LibraryMongoRepo mockLibraryMongoRepo;

    @Test
    public void test_library_employeeService_save(){

        
    Item testItem = new Item();
    testItem.setName("Test Loan");
    testItem.setOnLoan(true);
    testItem.setType("Book");
    testItem.setYear("2005");

    when(mockLibraryMongoRepo.save(testItem)).thenReturn(testItem);

    libraryService.save(testItem);
    }


    @Test
    public void test_libraryRepo_findAll(){
        Item testItem2 = new Item();
        testItem2.setName("ITEM2");
        testItem2.setOnLoan(false);
        testItem2.setType("Music");
        testItem2.setYear("1998");

        List<Item> listOfItems = new ArrayList<Item>();
        listOfItems.add(testItem2);

        when(mockLibraryMongoRepo.findAll()).thenReturn(listOfItems);
        assert(libraryService.findAll().size()>0);
        
    }

    
}
