package com.training.citi.library.service;

import com.training.citi.library.model.Item;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SpringBootTest
public class LibraryServiceTests {

    private static final Logger LOG = LoggerFactory.getLogger(LibraryService.class);

    @Autowired
    private LibraryService libraryService;

    @Test
    public void test_save_sanityCheck(){
    
    LOG.info("Testing Add Item");


    Item testItem = new Item();
    testItem.setName("Test Loan");
    testItem.setOnLoan(true);
    testItem.setType("Book");
    testItem.setYear("2005");

    libraryService.save(testItem);

    }

    @Test

    public void test_findAll_sanityCheck(){

        assert(libraryService.findAll().size() > 0);

    }


    
    
}
