package com.training.citi.library.model;

public class Item {

    private String type;
    private String name;
    private String year;
    private boolean onLoan;



    public Item() {
    }

    public Item(String type, String name, String year, boolean onLoan) {
        this.type = type;
        this.name = name;
        this.year = year;
        this.onLoan = onLoan;
    }
    

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public boolean isOnLoan() {
        return onLoan;
    }

    public void setOnLoan(boolean onLoan) {
        this.onLoan = onLoan;
    }

    @Override
    public String toString() {
        return "Item [name=" + name + ", onLoan=" + onLoan + ", type=" + type + ", year=" + year + "]";
    }

    

    

    

    
}
