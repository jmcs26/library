package com.training.citi.library.dao;

import com.training.citi.library.model.Item;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;


@Component
public interface LibraryMongoRepo extends MongoRepository<Item, String> {
    
}
