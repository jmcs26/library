package com.training.citi.library.service;

import java.util.List;

import com.training.citi.library.dao.LibraryMongoRepo;
import com.training.citi.library.model.Item;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LibraryService {
    
@Autowired
private LibraryMongoRepo mongoRepo;

public List<Item> findAll(){
    return mongoRepo.findAll();
}



public Item save (Item item){
    return mongoRepo.save(item);
}



}
